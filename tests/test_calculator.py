import random
import endpoints

import requests



def test_add_two_numbers():
    # Given
    first_number, second_number = random.randint(1, 1000), random.randint(1, 1000)
    expected_result = first_number + second_number
    # When
    body = {
        "firstNumber": first_number,
        "secondNumber": second_number
    }
    add_numbers_response = requests.post(endpoints.calculator_add, json=body)
    # Then
    assert add_numbers_response.status_code == 200

    result_dict = add_numbers_response.json()
    actual_result = result_dict['result']
    assert actual_result == expected_result
